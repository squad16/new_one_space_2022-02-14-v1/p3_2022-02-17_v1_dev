from setuptools import find_packages, setup

with open('version', 'r') as version_file:
    version = version_file.read().strip()

setup(
    name='p3_2022-02-17_v1_dev',
    packages=find_packages(where='src', exclude=['tests']),
    package_dir={'': 'src'},
    version=version,
    description='p3',
    author='po_1 po_1'
)
